﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using web_assignment_2020.DAL;
using web_assignment_2020.Models;

namespace web_assignment_2020.Controllers
{
    public class AircraftController : Controller
    {
        private List<string> availabilityList = new List<String> { "Operational", "Under Maintenance" };
        private AircraftDAL aircraftContext = new AircraftDAL();
        

        // GET: Aircraft
        public ActionResult Index()
        {
            if (HttpContext.Session.GetString("Role") == "Admin")
            {
                List<Aircraft> aircraftList = aircraftContext.GetAircraft();
                return View(aircraftList);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // GET: Aircraft/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Aircraft/Create
        public ActionResult Create()
        {
            ViewData["availabilityList"] = availabilityList;
            return View();
        }

        // POST: Aircraft/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Aircraft aircraft)
        {
            if (ModelState.IsValid)
            {
                aircraft.AircraftID = aircraftContext.Add(aircraft);
                return RedirectToAction("Index");
            }
            else
            {
                return View(aircraft);
            }
        }

        // POST: Aircraft/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Aircraft aircraft)
        {
            if (ModelState.IsValid)
            {
                aircraftContext.Update(aircraft);
                return RedirectToAction("Index");
            }
            else
            {
                return View(aircraft);
            }
        }

        public IActionResult Edit(int? id)
        {
            if (id != null)
            {
                List<Aircraft> aircraftList;
                aircraftList = aircraftContext.GetAircraft();
                foreach (Aircraft a in aircraftList)
                {
                    if (a.AircraftID == id)
                    {
                        ViewData["availabilityList"] = availabilityList;
                        return View(a);
                    }
                }
            }
            return RedirectToAction("_AircraftView");
        }

        // GET: Aircraft/Delete/5
        public ActionResult Delete(int id)
        {
            Aircraft aircraft = aircraftContext.GetDetails(id);
            return View(aircraft);
        }

        // POST: Aircraft/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Aircraft aircraft)
        {
            // Delete the staff record from database
            aircraftContext.Delete(aircraft.AircraftID);

            return RedirectToAction("Index");

        }

        // View of All Flight Schedules
        public IActionResult FlightScheduleView()
        {
            if (HttpContext.Session.GetString("Role") == "Admin")
            {
                List<FlightSchedule> flightSchedule = aircraftContext.GetAllFlightSchedule();
                return View(flightSchedule);
            }
            else
            {   
                return RedirectToAction("Index", "Home");
            }
        }

        // Select Aircraft to set to a certain Flight Schedule
        public ActionResult AssignAircraft(int? id)
        {
            if (id == null)
            { //Query string parameter not provided
              //Return to listing page, not allowed to edit
                return RedirectToAction("Create");
            }
            FlightSchedule flightSchedule = aircraftContext.GetFlightSchedules(id.Value);

            ViewData["aircraftIDs"] = aircraftContext.GetAllAircraftID();

            if (flightSchedule == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("Index");
            }
            
            return View(flightSchedule);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AssignAircraft(FlightSchedule flightSchedule)
        {
            if (ModelState.IsValid)
            {
                ViewData["aircraftIDs"] = aircraftContext.GetAllAircraftID();
                aircraftContext.AssignAircraft(flightSchedule);
                return RedirectToAction("FlightScheduleView");
            }
            else
            {
                //Input validation fails, return to the view
                //to display error message
                return View(flightSchedule);
            }
        }

    }
}