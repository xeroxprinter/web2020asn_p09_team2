﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data.SqlClient;
using web_assignment_2020.Models;
using System.IO.Pipelines;

namespace web_assignment_2020.DAL
{
    public class AircraftDAL
    {
        private IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public AircraftDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "Air_Flights_DB_ConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<Aircraft> GetAircraft()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM Aircraft ORDER BY AircraftID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a Aircraft list
            List<Aircraft> aircraftList = new List<Aircraft>();
            while (reader.Read())
            {
                aircraftList.Add(
                new Aircraft
                {
                    AircraftID = reader.GetInt32(0),
                    MakeModel = reader.GetString(1),
                    NumEconomySeat = reader.GetInt32(2),
                    NumBusinessSeat = reader.GetInt32(3),
                    DLM = reader.GetDateTime(4),
                    Status = reader.GetString(5),
                });
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return aircraftList;
        }

        public int Add(Aircraft aircraft)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO Aircraft(MakeModel,NumEconomySeat,NumBusinessSeat,DateLastMaintenance,Status)
      OUTPUT INSERTED.AircraftID
      VALUES(@makeModel,@numEconomySeat,@numBusinessSeat,@dateLastMaintenance,@status)";
            cmd.Parameters.AddWithValue("@makeModel", aircraft.MakeModel);
            cmd.Parameters.AddWithValue("@numEconomySeat", aircraft.NumEconomySeat);
            cmd.Parameters.AddWithValue("@numBusinessSeat", aircraft.NumBusinessSeat);
            cmd.Parameters.AddWithValue("@dateLastMaintenance", aircraft.DLM);
            cmd.Parameters.AddWithValue("@status", "Operational");
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated\
            //StaffID after executing the INSERT SQL statement
            aircraft.AircraftID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return aircraft.AircraftID;
        }

        public int Update(Aircraft aircraft)
        {
            FlightSchedule schedule = new FlightSchedule();
            DateTime datetime = new DateTime();

            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify an UPDATE SQL statement
            cmd.CommandText = @"SELECT * FROM FlightSchedule WHERE AircraftID = @updateAircraftID";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property. 
            cmd.Parameters.AddWithValue("@updateAircraftID", aircraft.AircraftID);
            conn.Open();

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    schedule.DepartureDateTime = !reader.IsDBNull(4) ?
                        reader.GetDateTime(4) : datetime;
                }
            }
            reader.Close();

            if (schedule.DepartureDateTime == DateTime.MinValue)
            {
                cmd.CommandText = @"UPDATE Aircraft SET Status = @status WHERE AircraftID = @selectedAircraftID";
                cmd.Parameters.AddWithValue("@selectedAircraftID", aircraft.AircraftID);
                cmd.Parameters.AddWithValue("@status", aircraft.Status);
            }

            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();

            //Close the database connection
            conn.Close();

            return count;
        }


        public Aircraft GetDetails(int AircraftID)
        {
            Aircraft Aircraft = new Aircraft();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement that
            //retrieves all attributes of a Aircraft record.
            cmd.CommandText = @"SELECT * FROM Aircraft WHERE AircraftID = @selectedAircraftID";
            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “AircraftId”.
            cmd.Parameters.AddWithValue("@selectedAircraftID", AircraftID);
            //Open a database connection
            conn.Open();
            //Execute SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    // Fill Aircraft object with values from the data reader
                    Aircraft.AircraftID = AircraftID;
                    Aircraft.MakeModel = !reader.IsDBNull(1) ? reader.GetString(1) : null;
                    // (char) 0 - ASCII Code 0 - null value
                    Aircraft.NumEconomySeat = !reader.IsDBNull(2) ?
                        reader.GetInt32(2) : (int?)null;
                    Aircraft.NumBusinessSeat = !reader.IsDBNull(3) ?
                        reader.GetInt32(3) : (int?)null;
                    Aircraft.DLM =
                        reader.GetDateTime(4);
                    Aircraft.Status = reader.GetString(5);
                }
            }
            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();
            return Aircraft;
        }

        public int Delete(int aircraftId)
        {

            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM FlightSchedule WHERE AircraftID = @selectedAircraftID";
            cmd.CommandText = @"DELETE FROM Aircraft WHERE AircraftID = @selectAircraftID";

            cmd.Parameters.AddWithValue("@selectAircraftID", aircraftId);
            cmd.Parameters.AddWithValue("@selectedAircraftID", aircraftId);
            //Open a database connection
            conn.Open();

            int rowAffected = 0;

            //Execute the DELETE SQL to remove the staff record
            rowAffected += cmd.ExecuteNonQuery();

            //Close database connection
            conn.Close();

            //Return number of row of staff record updated or deleted 
            return rowAffected;
        }

        public List<FlightSchedule> GetAllFlightSchedule()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM FlightSchedule ORDER BY ScheduleID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list
            List<FlightSchedule> flightSchedules = new List<FlightSchedule>();
            while (reader.Read())
            {
                flightSchedules.Add(
                new FlightSchedule
                {
                    ScheduleID = reader.GetInt32(0),
                    FlightNumber = reader.GetString(1),
                    RouteID = reader.GetInt32(2),
                    AircraftID = reader.GetInt32(3),
                    DepartureDateTime = reader.GetDateTime(4),
                    ArrivalDateTime = reader.GetDateTime(5),
                    EconomyClassPrice = reader.GetDecimal(6),
                    BusinessClassPrice = reader.GetDecimal(7),
                    Status = reader.GetString(8)
                });
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return flightSchedules;
        }

        public int AssignAircraft(FlightSchedule flightSchedule)
        {
            Aircraft aircraft = new Aircraft();
            FlightSchedule scheduleDate = new FlightSchedule();

            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Aircraft WHERE AircraftID = @selAircraftID";
            cmd.Parameters.AddWithValue("@selAircraftID", flightSchedule.AircraftID);
            //Open a database connection
            conn.Open();

            //Read data from Status to find out whether Aircraft is Available or not
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    aircraft.Status = reader.GetString(5);
                }
            }
            reader.Close();

            cmd.CommandText = @"SELECT * FROM FlightSchedule WHERE ScheduleID = @selScheduleID";
            cmd.Parameters.AddWithValue("@selScheduleID", flightSchedule.ScheduleID);

            //Read data from Departure Date of Flight Schedule
            SqlDataReader reader2 = cmd.ExecuteReader();
            if (reader2.HasRows)
            {
                while (reader2.Read())
                {
                    scheduleDate.DepartureDateTime = reader2.GetDateTime(5);
                }
            }
            reader2.Close();

            if (scheduleDate.DepartureDateTime >= DateTime.Today)
            {
                if (aircraft.Status == "Operational")
                {
                    //Change Aircraft ID to User's Input
                    cmd.CommandText = @"UPDATE FlightSchedule SET AircraftID = @aircraftID WHERE ScheduleID = @scheduleID";

                    //Define the parameters used in SQL statement, value for each parameter
                    //is retrieved from respective class's property. 
                    cmd.Parameters.AddWithValue("@aircraftID", flightSchedule.AircraftID);
                    cmd.Parameters.AddWithValue("@scheduleID", flightSchedule.ScheduleID);
                }
            }

            //ExecuteNonQuery is used for UPDATE and DELETE
            int count = cmd.ExecuteNonQuery();

            //Close the database connection
            conn.Close();
            return count;
        }

        public FlightSchedule GetFlightSchedules(int flightID)
        {
            FlightSchedule schedule = new FlightSchedule();

            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();

            //Specify the SELECT SQL statement that
            //retrieves all attributes of a staff record.
            cmd.CommandText = @"SELECT * FROM FlightSchedule WHERE ScheduleID = @selectedScheduleID";

            //Define the parameter used in SQL statement, value for the
            //parameter is retrieved from the method parameter “staffId”.
            cmd.Parameters.AddWithValue("@selectedScheduleID", flightID);

            //Open a database connection
            conn.Open();

            //Execute SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            DateTime datetime = new DateTime();
            if (reader.HasRows)
            {
                //Read the record from database
                while (reader.Read())
                {
                    // Fill staff object with values from the data reader
                    schedule.ScheduleID = flightID;

                    schedule.FlightNumber = !reader.IsDBNull(1) ? reader.GetString(1) : null;
                    schedule.RouteID = !reader.IsDBNull(2) ?
                                            reader.GetInt32(2) : (int)0;
                    schedule.AircraftID = !reader.IsDBNull(3) ?
                                            reader.GetInt32(3) : (int)0;
                    schedule.DepartureDateTime = !reader.IsDBNull(4) ?
                        reader.GetDateTime(4) : datetime;
                    schedule.ArrivalDateTime = !reader.IsDBNull(5) ?
                        reader.GetDateTime(5) : datetime;
                    schedule.EconomyClassPrice = !reader.IsDBNull(6) ? reader.GetDecimal(6) : (int)0;
                    schedule.BusinessClassPrice = !reader.IsDBNull(7) ? reader.GetDecimal(7) : (int)0;
                    schedule.Status = !reader.IsDBNull(8) ? reader.GetString(8) : null;



                }
            }
            //Close data reader
            reader.Close();
            //Close database connection
            conn.Close();

            return schedule;
        }

        public List<int> GetAllAircraftID()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT AircraftID FROM Aircraft WHERE Status = @Operational";
            cmd.Parameters.AddWithValue("@Operational", "Operational");
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list
            List<int> aircraftIDs = new List<int>();
            while (reader.Read())
            {
                aircraftIDs.Add(reader.GetInt32(0));
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return aircraftIDs;
        }
    }
}
