#pragma checksum "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fb0f3a43bfe2226b435aacc9987a78dd517c4303"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_FlightRoute_Details), @"mvc.1.0.view", @"/Views/FlightRoute/Details.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\_ViewImports.cshtml"
using web_assignment_2020;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\_ViewImports.cshtml"
using web_assignment_2020.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fb0f3a43bfe2226b435aacc9987a78dd517c4303", @"/Views/FlightRoute/Details.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9442a9bc52765442572313f41bee9369ecbde59f", @"/Views/_ViewImports.cshtml")]
    public class Views_FlightRoute_Details : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<web_assignment_2020.Models.FlightScheduleViewModel>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n<h4 class=\"PageTitle\">Flight Schedules for Route ID: ");
#nullable restore
#line 3 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
                                                Write(ViewData["routeID"]);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </h4>\r\n\r\n");
#nullable restore
#line 5 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
 if (Model.Count() > 0)
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <table class=\"table\">\r\n        <thead>\r\n            <tr>\r\n                <th>\r\n                    ");
#nullable restore
#line 11 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
               Write(Html.DisplayNameFor(model => model.ScheduleID));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
#nullable restore
#line 14 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
               Write(Html.DisplayNameFor(model => model.FlightNumber));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
#nullable restore
#line 17 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
               Write(Html.DisplayNameFor(model => model.RouteID));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
#nullable restore
#line 20 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
               Write(Html.DisplayNameFor(model => model.AircraftID));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
#nullable restore
#line 23 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
               Write(Html.DisplayNameFor(model => model.DepartureDateTime));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
#nullable restore
#line 26 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
               Write(Html.DisplayNameFor(model => model.ArrivalDateTime));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
#nullable restore
#line 29 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
               Write(Html.DisplayNameFor(model => model.EconomyClassPrice));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
#nullable restore
#line 32 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
               Write(Html.DisplayNameFor(model => model.BusinessClassPrice));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </th>\r\n                <th>\r\n                    ");
#nullable restore
#line 35 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
               Write(Html.DisplayNameFor(model => model.Status));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </th>\r\n                <th></th>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n");
#nullable restore
#line 41 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
             foreach (var item in Model)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <tr>\r\n                    <td>\r\n                        ");
#nullable restore
#line 45 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
                   Write(Html.DisplayFor(modelItem => item.ScheduleID));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 48 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
                   Write(Html.DisplayFor(modelItem => item.FlightNumber));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 51 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
                   Write(Html.DisplayFor(modelItem => item.RouteID));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 54 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
                   Write(Html.DisplayFor(modelItem => item.AircraftID));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 57 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
                   Write(Html.DisplayFor(modelItem => item.DepartureDateTime));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 60 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
                   Write(Html.DisplayFor(modelItem => item.ArrivalDateTime));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 63 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
                   Write(Html.DisplayFor(modelItem => item.EconomyClassPrice));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 66 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
                   Write(Html.DisplayFor(modelItem => item.BusinessClassPrice));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
#nullable restore
#line 69 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Status));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </td>\r\n\r\n                </tr>\r\n");
#nullable restore
#line 73 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </tbody>\r\n    </table>\r\n");
#nullable restore
#line 76 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"

}
else
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <h5 style=\"text-align: center;font-weight: bold;color: red;margin-top:250px;margin-bottom:250px;\" >No flight schedules for selected flight route!</h5>\r\n");
#nullable restore
#line 81 "D:\web_assignment\web_assignment_2020\web_assignment_2020\Views\FlightRoute\Details.cshtml"
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<web_assignment_2020.Models.FlightScheduleViewModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
