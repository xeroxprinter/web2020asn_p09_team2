﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_assignment_2020.Models
{
    public class AssignAircraft : Aircraft
    {
        [Display(Name = "ScheduleID")]
        public int ScheduleID { get; set; }
    }
}
