﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;

namespace web_assignment_2020.Models
{
    public class FlightScheduleViewModel
    {
        [Display(Name = "Schedule ID")]
        public int ScheduleID { get; set; }

        [Display(Name = "Flight Number")]
        public string FlightNumber { get; set; }

        [Display(Name = "Route ID")]
        public int RouteID { get; set; }

        [Display(Name = "Aircraft ID")]
        public int AircraftID { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Departure DateTime")]
        public DateTime DepartureDateTime { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Arrival DateTime")]
        public DateTime ArrivalDateTime { get; set; }

        [Display(Name = "Economy Class Price")]
        public decimal EconomyClassPrice { get; set; }

        [Display(Name = "Business Class Price")]
        public decimal BusinessClassPrice { get; set; }

        public string Status { get; set; }
    }
}
