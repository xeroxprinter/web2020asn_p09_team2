﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_assignment_2020.Models
{
    public class FlightRoute
    {
        [Display(Name = "Route ID")]
        public int RouteID { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Required, StringLength – cannot exceed 50 characters")]
        [Display(Name = "Departure City")]
        [DataType(DataType.Text)]
        public string DepartureCity { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Required, StringLength – cannot exceed 50 characters")]
        [DataType(DataType.Text)]
        [Display(Name = "Departure Country")]
        public string DepartureCountry { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Required, StringLength – cannot exceed 50 characters")]
        [Display(Name = "Arrival City")]
        [DataType(DataType.Text)]
        public string ArrivalCity { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Required, StringLength – cannot exceed 50 characters")]
        [DataType(DataType.Text)]
        [Display(Name = "Arrival Country")]
        public string ArrivalCountry { get; set; }

        [Display(Name = "Flight Duration")]
        public int FlightDuration { get; set; }

    }
}
