﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_assignment_2020.Models
{
    public class Aircraft
    {
        [Display(Name = "ID")]
        public int AircraftID { get; set; }

        [Required(ErrorMessage = "Please Enter an Airplane Model!")]
        [Display(Name = "Model:")]
        public string MakeModel { get; set; }

        [Required(ErrorMessage = "Please specify Number of Seats!")]
        [Display(Name = "Economy Seats:")]
        public int? NumEconomySeat { get; set; }

        [Required(ErrorMessage = "Please specify Number of Seats!")]
        [Display(Name = "Business Seats:")]
        public int? NumBusinessSeat { get; set; }

        [Required(ErrorMessage = "Please enter a date")]
        [Display(Name = "Date of Last Maintenace:")]
        [DataType(DataType.DateTime)]
        public DateTime DLM { get; set; }

        [Display(Name = "Aircraft Status:")]
        public string Status { get; set; }

        public Aircraft()
        {
            Status = "Operational";
        }
    }
}
