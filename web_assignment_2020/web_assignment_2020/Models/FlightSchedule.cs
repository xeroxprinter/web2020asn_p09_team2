﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;

namespace web_assignment_2020.Models
{
    public class FlightSchedule
    {
        [Display(Name = "Schedule ID")]
        public int ScheduleID { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Required, StringLength – cannot exceed 20 characters")]
        [Display(Name = "Flight Number")]
        public string FlightNumber { get; set; }

        [Required]
        [Display(Name = "Route ID")]
        public int RouteID { get; set; }

        [Display(Name = "Aircraft ID")]
        public int AircraftID { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}")]
        [Display(Name = "Departure DateTime")]
        public DateTime DepartureDateTime { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm}")]
        [Display(Name = "Arrival DateTime")]
        public DateTime ArrivalDateTime { get; set; }

        [Required]
        [Display(Name ="Economy Class Price")]
        public decimal EconomyClassPrice { get; set; }

        [Required]
        [Display(Name = "Business Class Price")]
        public decimal BusinessClassPrice { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Required, StringLength – cannot exceed 20 characters")]
        public string Status { get; set; }


        public FlightSchedule()
        {
            Status = "Opened";
        }
    }
}
